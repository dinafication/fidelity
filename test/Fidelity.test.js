const assert = require('assert');
const ganache = require('ganache-cli');
const Web3 = require('web3');

const provider = ganache.provider();
const web3 = new Web3(provider);

const { interface, bytecode } = require('../compile');

let accounts;
let fidelity;
let placeholder;

const totalSupply = 1000000;
const fee = 1;

beforeEach(async () => {
     // get a list of all accounts from ganache
    accounts = await web3.eth.getAccounts();

     // use one of those accounts

     // the contract
     fidelity = await new web3.eth.Contract(JSON.parse(interface))
     .deploy({ data: bytecode, arguments: [totalSupply , fee]})
     .send({from: accounts[0], gas:'1000000'});

     fidelity.setProvider(provider);

     // one account needs to have initial tokens to transfer
     await fidelity.methods.transfer(accounts[1], 100).send({
       from: accounts[0]
     });

     placeholder = await fidelity.methods.balanceOf(accounts[1]).call();
     console.log('Account1 has balance of: ' + placeholder);
});


describe('fidelity', () => {
  it('initializes the contract with the correct values', async () => {

     placeholder =  await fidelity.methods.name().call();
     assert.equal('Fidelity', placeholder);

     placeholder =  await fidelity.methods.symbol().call();
     assert.equal('FID', placeholder);

     placeholder =  await fidelity.methods.totalSupply().call();
     assert.equal(totalSupply, placeholder);

     placeholder =  await fidelity.methods.fee().call();
     assert.equal(fee, placeholder);

  });

  it('transfer', async () => {

     let initialBalance1 = await fidelity.methods.balanceOf(accounts[1]).call();
     console.log('Account1 has a balance of: ' + initialBalance1);

     let initialBalanceMng = await fidelity.methods.balanceOf(accounts[0]).call();
     console.log('Account0 has a balance of: ' + initialBalanceMng);

     // transfer from acc1 to manager
     await fidelity.methods.transfer(accounts[0], 10).send({
       from: accounts[1]
     });

     let finalBalance1 = await fidelity.methods.balanceOf(accounts[1]).call();
     console.log('Account1 has a balance of: ' + finalBalance1);

     let finalBalanceMng = await fidelity.methods.balanceOf(accounts[0]).call();
     console.log('Account0 has a balance of: ' + finalBalanceMng);

     assert(initialBalanceMng - finalBalanceMng == -10);
     assert(initialBalance1 - finalBalance1 == 10);

     // transfer from acc1 to acc2
     initialBalance1 = await fidelity.methods.balanceOf(accounts[1]).call();
     console.log('Account1 has a balance of: ' + initialBalance1);

     initialBalanceMng = await fidelity.methods.balanceOf(accounts[0]).call();
     console.log('Account0 has a balance of: ' + initialBalanceMng);

     let initialBalance2 = await fidelity.methods.balanceOf(accounts[2]).call();
     console.log('Account2 has a balance of: ' + initialBalance2);

     // transfer from acc1 to acc2
     await fidelity.methods.transfer(accounts[2], 5).send({
       from: accounts[1]
     });

     finalBalance1 = await fidelity.methods.balanceOf(accounts[1]).call();
     console.log('Account1 has a balance of: ' + finalBalance1);

     let finalBalance2 = await fidelity.methods.balanceOf(accounts[2]).call();
     console.log('Account2 has balance of: ' + finalBalance2);

     finalBalanceMng = await fidelity.methods.balanceOf(accounts[0]).call();
     console.log('Account0 has a balance of: ' + finalBalanceMng);

     assert(finalBalance1 - initialBalance1 == -5);
     assert(finalBalance2 - initialBalance2 == 5 - fee);
     assert(finalBalanceMng - initialBalanceMng == fee);

  });

  it('transfer from', async () => {

     let initialBalance1 = await fidelity.methods.balanceOf(accounts[1]).call();
     console.log('Account1 has a balance of: ' + initialBalance1);

     let initialBalanceMng = await fidelity.methods.balanceOf(accounts[0]).call();
     console.log('Account0 has a balance of: ' + initialBalanceMng);

     // transfer from acc1 to manager(acc0)
     await fidelity.methods.transferFrom(accounts[1], accounts[0], 10).send({
       from: accounts[1]
     });

     let finalBalance1 = await fidelity.methods.balanceOf(accounts[1]).call();
     console.log('Account1 has a balance of: ' + finalBalance1);

     let finalBalanceMng = await fidelity.methods.balanceOf(accounts[0]).call();
     console.log('Account0 has a balance of: ' + finalBalanceMng);

     assert(initialBalanceMng - finalBalanceMng == -10);
     assert(initialBalance1 - finalBalance1 == 10);

     // transfer from acc1 to acc2
     initialBalance1 = await fidelity.methods.balanceOf(accounts[1]).call();
     console.log('Account1 has a balance of: ' + initialBalance1);

     initialBalanceMng = await fidelity.methods.balanceOf(accounts[0]).call();
     console.log('Account0 has a balance of: ' + initialBalanceMng);

     let initialBalance2 = await fidelity.methods.balanceOf(accounts[2]).call();
     console.log('Account2 has a balance of: ' + initialBalance2);

     // transfer from acc1 to acc2
     await fidelity.methods.transferFrom(accounts[1], accounts[2], 5).send({
       from: accounts[1]
     });

     finalBalance1 = await fidelity.methods.balanceOf(accounts[1]).call();
     console.log('Account1 has a balance of: ' + finalBalance1);

     let finalBalance2 = await fidelity.methods.balanceOf(accounts[2]).call();
     console.log('Account2 has balance of: ' + finalBalance2);

     finalBalanceMng = await fidelity.methods.balanceOf(accounts[0]).call();
     console.log('Account0 has a balance of: ' + finalBalanceMng);

     assert(finalBalance1 - initialBalance1 == -5);
     assert(finalBalance2 - initialBalance2 == 5 - fee);
     assert(finalBalanceMng - initialBalanceMng == fee);

  });

});
