pragma solidity ^0.4.17;


contract ERC20Interface {
   function totalSupply() public constant returns (uint);
   function balanceOf(address tokenOwner) public constant returns (uint balance);
   function allowance(address tokenOwner, address spender) public constant returns (uint remaining);
   function transfer(address to, uint tokens) public returns (bool success);
   function approve(address spender, uint tokens) public returns (bool success);
   function transferFrom(address from, address to, uint tokens) public returns (bool success);

   event Transfer(address indexed from, address indexed to, uint tokens);
   event Approval(address indexed tokenOwner, address indexed spender, uint tokens);
}

contract FidelityInterface {
   function enter() public;

}

contract Fidelity is ERC20Interface, FidelityInterface{

   string public symbol;
   string public  name;
   uint public totalSupply;

   uint public fee;

   address public manager;

   mapping(address => uint) balances;
   mapping(address => mapping(address => uint)) allowed;

   // ------------------------------------------------------------------------
   // Constructor
   // ------------------------------------------------------------------------
   function Fidelity(uint _totalSupply, uint _fee) public {
       symbol = "FID";
       name = "Fidelity";
       totalSupply = _totalSupply;
       balances[msg.sender] = _totalSupply;
       fee = _fee;

       manager = msg.sender;
       emit Transfer(address(0), msg.sender, totalSupply);
   }


   // ------------------------------------------------------------------------
   // Total supply
   // ------------------------------------------------------------------------
   function totalSupply() public constant returns (uint) {
       return totalSupply  - balances[address(0)];
   }


   // ------------------------------------------------------------------------
   // Get the token balance for account tokenOwner
   // ------------------------------------------------------------------------
   function balanceOf(address tokenOwner) public constant returns (uint balance) {
       return balances[tokenOwner];
   }


   // ------------------------------------------------------------------------
   // Transfer the balance from token owner's account to to account
   // - Owner's account must have sufficient balance to transfer
   // - 0 value transfers are allowed
   // ------------------------------------------------------------------------
   function transfer(address to, uint tokens) public returns (bool success) {
      require(balances[msg.sender] >= tokens);

      // sending to another person
      if(to != manager && msg.sender != manager) {
        balances[msg.sender] -= tokens;

        balances[to] += tokens - fee;
        balances[manager] += fee;
      }

      // sending tokenst to or from Fidelity owner = manager
      else {
        balances[msg.sender] -= tokens;
        balances[to] += tokens;
      }

       Transfer(msg.sender, to, tokens-fee);
       Transfer(msg.sender, manager, fee);

       return true;
   }


   // ------------------------------------------------------------------------
   // Token owner can approve for spender to transferFrom(...) tokens
   // from the token owner's account
   //
   // https://github.com/ethereum/EIPs/blob/master/EIPS/eip-20-token-standard.md
   // recommends that there are no checks for the approval double-spend attack
   // as this should be implemented in user interfaces
   // ------------------------------------------------------------------------
   function approve(address spender, uint tokens) public returns (bool success) {
       allowed[msg.sender][spender] = tokens;
       emit Approval(msg.sender, spender, tokens);
       return true;
   }


   // ------------------------------------------------------------------------
   // Transfer tokens-fee from the from account to the to account and
   // fee to the manager account
   //
   // The calling account must already have sufficient tokens approve(...)-d
   // for spending from the from account and
   // - From account must have sufficient balance to transfer
   // - Spender must have sufficient allowance to transfer
   // - 0 value transfers are allowed
   // ------------------------------------------------------------------------
   function transferFrom(address from, address to, uint tokens) public returns (bool success) {
       balances[from] = balances[from] - tokens;
       balances[to] = balances[to] + tokens - fee;
       balances[manager] = balances[manager] + fee;

       Transfer(from, to, tokens-fee);
       Transfer(from, manager, fee);
       return true;
   }


   // ------------------------------------------------------------------------
   // Returns the amount of tokens approved by the owner that can be
   // transferred to the spender's account
   // ------------------------------------------------------------------------
   function allowance(address tokenOwner, address spender) public constant returns (uint remaining) {
       return allowed[tokenOwner][spender];
   }

   function enter() public {

      uint tokens = 1;
      if(balances[msg.sender] % 10 == 0) {
        tokens++;
      }

        transferFrom(manager, msg.sender, tokens);
   }


}
