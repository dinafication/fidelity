const HDWalletProvider = require('truffle-hdwallet-provider');
const Web3 = require('web3');
const {interface, bytecode } = require('./compile');

const provider = new HDWalletProvider(
    'jelly balance stone super trade mobile device couple diet edge bone peanut',
    'https://rinkeby.infura.io/v3/06dadff57bdc4d7696f2c7b51027f26e'
);

const web3 = new Web3(provider);

const deploy = async () => {
  const accounts = await web3.eth.getAccounts();

  console.log('Attempting to deploy from account', accounts[0]);

  const result = await new web3.eth.Contract(JSON.parse(interface))
  .deploy({ data: bytecode, arguments: [10])
  .send({ gas: '1000000', from: accounts[0] });

  console.log('contract deployed to', result.options.address);
};
deploy();
